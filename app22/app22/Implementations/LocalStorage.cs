﻿using System;
using System.Threading.Tasks;
using app22.Core.Services;
using Xamarin.Essentials;

namespace app22.Implementations
{
	public class LocalStorage : ILocalStorage
    {
        public const string SEARCH_KEY = "search_key";

		public async Task SaveSearch(string search)
		{
            try
            {
                await SecureStorage.SetAsync(SEARCH_KEY, search);
            }
            catch (Exception ex)
            {
                // Possible that device doesn't support secure storage on device.
            }
        }

        public async Task<string> GetSearch()
        {
            try
            {
                var search = await SecureStorage.GetAsync(SEARCH_KEY);
                return search;
            }
            catch (Exception ex)
            {
                return "";
                // Possible that device doesn't support secure storage on device.
            }
        }
	}
}

