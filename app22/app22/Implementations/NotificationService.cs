﻿using System;
using System.Linq;
using System.Threading.Tasks;
using app22.Core.Services;
using Xamarin.Forms;

namespace app22.Implementations
{
    public class NotificationService : INotificationService
    {
        public Task ShowError(string message)
        {
            var page = Application.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            return page.DisplayAlert("Ошибка", message, "Ок");
        }
    }
}

