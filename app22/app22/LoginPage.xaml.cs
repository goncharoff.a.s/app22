﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using app22.Core;
using app22.Core.ViewModels;
using Xamarin.Forms;

namespace app22
{
    public partial class LoginPage : ContentPage, ILoginPage
    {

        LoginPageVM _viewModel;
        public LoginPage()
        {
            InitializeComponent();

            _viewModel = new LoginPageVM(this);
            this.BindingContext = _viewModel;
        }

        public void ShowNextPage()
        {
            this.Navigation.PushAsync(new NewsPage());
        }
    }
}

