﻿using System;
using System.Collections.Generic;
using app22.Core;
using Xamarin.Forms;

namespace app22
{
    public partial class NewsItemPage : ContentPage
    {
        public NewsItemPage(NewsItemPageVM pageVM)
        {
            InitializeComponent();

            this.BindingContext = pageVM;
        }
    }
}

