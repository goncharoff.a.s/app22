﻿using System;
using System.Collections.Generic;
using app22.Core;
using app22.Core.ViewModels.NewsPage;
using Xamarin.Forms;

namespace app22
{
    public partial class NewsPage : ContentPage, INewsPage
    {
        public NewsPage()
        {
            InitializeComponent();

            this.BindingContext = new NewsPageVM(this);
        }

        public void ShowNewsItemPage(NewsItemPageVM pageVM)
        {
            this.Navigation.PushAsync(new NewsItemPage(pageVM));
        }
    }
}

