﻿using System;
using app22.Core.Services;
using app22.Core.Services.Fakes;
using app22.Core.Services.Implementations;
using app22.Implementations;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace app22
{
    public partial class App : Application
    {
        public App ()
        {
            /*
             * HttpClient (авторизация)
             * Рефакторинг
             * LocalStorage
             * Image for News
             */


            RegisterServices(enableFakse: false);

            InitializeComponent();

            //if(Session.IsAlive)
            //    //satrt main pahe
            //    else
            //        //start login page


            MainPage = new NavigationPage(new NewsPage());
        }

        private void RegisterServices(bool enableFakse)
        {
            if (enableFakse)
            {
                Service<ILoginService>.Register(new FakeLoginService());
                Service<INewsService>.Register(new FakeNewsService());
                //ServiceLocator.RegisterService(typeof(ILoginService), new FakeLoginService());
            }
            else
            {
                Service<ILoginService>.Register(new FakeLoginService());
                Service<INewsService>.Register(new NewsService()); 
                Service<IConnectionService>.Register(new ConnectionService());
                //ServiceLocator.RegisterService(typeof(ILoginService), new LoginService());
            }

            Service<INotificationService>.Register(new NotificationService()); 
            Service<ILocalStorage>.Register(new LocalStorage());
        }

        protected override void OnStart ()
        {
        }

        protected override void OnSleep ()
        {
        }

        protected override void OnResume ()
        {
        }
    }
}

