﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using app22.Core.Services;
using app22.Core.Services.Fakes;
using app22.Core.ViewModels;

namespace app22.Core
{
    public class LoginPageVM : BasePageVM
    {
        private string _loginEditorText;
        public string LoginEditorText //public string LoginEditorText { get; set; }
        {
            get
            {
                return _loginEditorText;
            }
            set
            {
                if (_loginEditorText != value)
                {
                    _loginEditorText = value;
                    PropertyChangedInvoke(this, new PropertyChangedEventArgs(nameof(LoginEditorText)));
                }
            }
        }

        private string _passEditorText;
        public string PassEditorText
        {
            get
            {
                return _passEditorText;
            }
            set
            {
                if (_passEditorText != value)
                {
                    _passEditorText = value;
                    PropertyChangedInvoke(this, new PropertyChangedEventArgs(nameof(PassEditorText)));
                }
            }
        }

        private bool _loginEnabled = true;
        public bool LoginEnabled
        {
            get
            {
                return _loginEnabled;
            }
            set
            {
                if (_loginEnabled != value)
                {
                    _loginEnabled = value;
                    PropertyChangedInvoke(this, new PropertyChangedEventArgs(nameof(LoginEnabled)));
                }
            }
        }

        public ICommand LoginButton { get; set; }

        private ILoginPage _loginPage;

        public LoginPageVM(ILoginPage loginPage)
        {
            _loginPage = loginPage;
            LoginButton = new Command(LoginButtonExecute);

#if DEBUG
            LoginEditorText = "123";
            PassEditorText = "321";
#endif
        }

        private async void LoginButtonExecute()
        {
            var login = LoginEditorText;
            var pass = PassEditorText;

            //var loginService = (ILoginService)ServiceLocator.GetService(typeof(ILoginService));
            //var loginService = Service<ILoginService>.GetInstance();

            try
            {
                LoginEnabled = false;
                IsRunning = true;
                var user = await Context.LoginService.Login(login, pass);

                _loginPage.ShowNextPage();
            }
            catch (Exception ex)
            {
                await Context.Notification.ShowError(ex.ToString());
            }
            finally
            {
                IsRunning = false;
                LoginEnabled = true;
            }
        }
    }
}