﻿using System;
using System.ComponentModel;

namespace app22.Core
{
    public abstract class BasePageVM : INotifyPropertyChanged
    {
        private bool _isRunning;
        public bool IsRunning
        {
            get
            {
                return _isRunning;
            }
            set
            {
                if (_isRunning != value)
                {
                    _isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsRunning)));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void PropertyChangedInvoke(object sender, PropertyChangedEventArgs args)
        {
            PropertyChanged?.Invoke(sender, args);
        }
    }
}

