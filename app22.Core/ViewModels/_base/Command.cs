﻿using System;
using System.Windows.Input;

namespace app22.Core.ViewModels
{
    public class Command : ICommand
    {
        private Action _execute;
        public Command(Action execute)
        {
            _execute = execute;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return _execute != null;
        }

        public void Execute(object parameter)
        {
            _execute?.Invoke();
        }
    }
}

