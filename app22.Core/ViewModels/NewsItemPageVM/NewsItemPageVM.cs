﻿using System;
using System.ComponentModel;
using app22.Core.Services.DTOs;

namespace app22.Core
{
    public class NewsItemPageVM : BasePageVM
    {
        private string _title;
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (_title != value)
                {
                    _title = value;
                    PropertyChangedInvoke(this, new PropertyChangedEventArgs(nameof(Title)));
                }
            }
        }

        private string _text;

        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                if (_text != value)
                {
                    _text = value;
                    PropertyChangedInvoke(this, new PropertyChangedEventArgs(nameof(Text)));
                }
            }
        }

        public string ImageUrl { get; set; }

        private NewsDTO _newsDTO;
        public NewsItemPageVM(NewsDTO newsDTO)
        {
            _newsDTO = newsDTO;

            Title = newsDTO.Title;

            Text = newsDTO.Content;

            ImageUrl = newsDTO.UrlToImage;
        }
    }
}

