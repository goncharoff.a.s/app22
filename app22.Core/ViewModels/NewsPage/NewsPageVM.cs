﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using app22.Core.Services;
using app22.Core.ViewModels.NewsPage;

namespace app22.Core
{
    public class NewsPageVM : BasePageVM
    {

        private string _search;
        public string Search
        {
            get
            {
                return _search;
            }
            set
            {
                if (_search != value)
                {
                    _search = value;
                    PropertyChangedInvoke(this, new PropertyChangedEventArgs(nameof(Search)));
                }
            }
        }

        private ObservableCollection<NewsItemVM> _items;
        public ObservableCollection<NewsItemVM> Items
        {
            get
            {
                return _items;
            }
            set
            {
                if (_items != value)
                {
                    _items = value;
                    PropertyChangedInvoke(this, new PropertyChangedEventArgs(nameof(Items)));
                }
            }
        }

        private NewsItemVM _selectedItem;
        public NewsItemVM SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                if (_selectedItem != value)
                {
                    _selectedItem = value;
                    PropertyChangedInvoke(this, new PropertyChangedEventArgs(nameof(SelectedItem)));
                }
            }
        }

        private INewsPage _newsPage;

        public NewsPageVM(INewsPage newsPage)
        {
            _newsPage = newsPage;
            //LoadNews();

            Items = new ObservableCollection<NewsItemVM>();

            PropertyChanged += NewsPageVMPropertyChanged;

            RestoreSearch();
        }

        private async void RestoreSearch()
        {
            var search = await Context.Storage.GetSearch();
            Search = search;
        }

        private void NewsPageVMPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedItem))
            {
                SelectedItemChanged();
            }
            if (e.PropertyName == nameof(Search))
            {
                SearchChanged();
            }
        }

        public void SelectedItemChanged()
        {
            if (SelectedItem == null)
                return;

            var newsItemPageVM = new NewsItemPageVM(SelectedItem.NewsDTO);

            _newsPage.ShowNewsItemPage(newsItemPageVM);

            SelectedItem = null;
        }

        public void SearchChanged()
        {
            Context.Storage.SaveSearch(Search);

            if (Search.Length < 3)
            {
                if (_cts != null)
                {
                    _cts.Cancel();
                    _cts = null;
                }
                Items.Clear();
                return;
            }

            LoadNews(Search);
        }

        private CancellationTokenSource _cts;
        private async void LoadNews(string search)
        {
            try
            {
                if (_cts != null)
                {
                    _cts.Cancel();
                    _cts = null;
                }
                _cts = new CancellationTokenSource();

                this.IsRunning = true;
                var newsResult = await Context.NewsService.LoadNews(search, _cts.Token);

                Items.Clear();

                foreach (var item in newsResult.Articles)
                {
                    Items.Add(new NewsItemVM(item)
                    {
                        Title = item.Title,
                        Description = item.Description,
                        ImageUrl = item.UrlToImage
                    });
                }
            }
            catch (Exception ex)
            {
                if (ex is OperationCanceledException)
                    return;

                await Context.Notification.ShowError(ex.ToString());
                //LoadNews(Search);
            }
            finally
            {
                this.IsRunning = false;
            }
        }
    }
}

