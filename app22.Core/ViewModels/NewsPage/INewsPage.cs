﻿using System;
namespace app22.Core
{
    public interface INewsPage
    {
        public void ShowNewsItemPage(NewsItemPageVM pageVM);
    }
}

