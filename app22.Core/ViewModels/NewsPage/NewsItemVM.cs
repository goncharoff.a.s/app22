﻿using System;
using app22.Core.Services.DTOs;

namespace app22.Core.ViewModels.NewsPage
{
    public class NewsItemVM
    {
        public NewsDTO NewsDTO { get; private set; }

        public NewsItemVM(NewsDTO newsDTO)
        {
            NewsDTO = newsDTO;
        }

        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
    }
}

