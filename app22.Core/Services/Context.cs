﻿using System;
namespace app22.Core.Services
{
    public static class Context
    {
        public static ILoginService LoginService = Service<ILoginService>.GetInstance();
        public static INewsService NewsService = Service<INewsService>.GetInstance();
        public static IConnectionService Connection = Service<IConnectionService>.GetInstance();
        public static INotificationService Notification = Service<INotificationService>.GetInstance();
        public static ILocalStorage Storage = Service<ILocalStorage>.GetInstance();
    }
}

