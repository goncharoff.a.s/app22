﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace app22.Core.Services
{
    public interface IConnectionService
    {
        public Task<string> GetRequest(string url, CancellationToken token = default);
    }
}

