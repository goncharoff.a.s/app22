﻿using System;
namespace app22.Core.Services.DTOs
{
    public class UserDTO
    {
        public long Id { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Age { get; set; }
    }
}

