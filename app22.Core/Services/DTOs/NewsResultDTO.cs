﻿using System;
using System.Collections.Generic;

namespace app22.Core.Services.DTOs
{
    public class NewsResultDTO
    {
        public string Status { get; set; }
        public int TotalResults { get; set; }
        public List<NewsDTO> Articles { get; set; }
    }
}

