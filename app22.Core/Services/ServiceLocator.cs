﻿using System;
using System.Collections.Generic;

namespace app22.Core.Services
{
    public static class ServiceLocator
    {
        private static Dictionary<Type, object> _services = new Dictionary<Type, object>();

        public static void RegisterService(Type type, object _instance)
        {
            _services[type] = _instance;
        }

        public static object GetService(Type type)
        {
            return _services[type];
        }
    }
}

