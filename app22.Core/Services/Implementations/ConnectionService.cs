﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using app22.Core.Services.DTOs;
using Newtonsoft.Json;

namespace app22.Core.Services.Implementations
{
    public class ConnectionService : IConnectionService
    {
        public async Task<string> GetRequest(string url, CancellationToken token)
        {
            var client = new HttpClient( new HttpClientHandler() {ServerCertificateCustomValidationCallback = (a,b,c,d) => true });
            client.Timeout = TimeSpan.FromSeconds(60);
            client.DefaultRequestHeaders.Add("User-Agent", "C# App");

            var response = await client.GetAsync(url, token);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var json = await response.Content.ReadAsStringAsync();

                return json;
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();

                var error = response.ReasonPhrase;
                throw new Exception("Запрос выполнился неуспешно." + error);
            }
        }
    }
}

