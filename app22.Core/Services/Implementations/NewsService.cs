﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using app22.Core.Services.DTOs;
using Newtonsoft.Json;

namespace app22.Core.Services.Implementations
{
    public class NewsService : INewsService
    {
        public async Task<NewsResultDTO> LoadNews(string search, CancellationToken token)
        {
            string query = search;// "tesla";

            var json = await Context.Connection.GetRequest($"https://newsapi.org/v2/everything?q={query}&from=2022-11-15&sortBy=publishedAt&apiKey=1586c20f20fb4718920b326d9826ab28", token);
            var dto = JsonConvert.DeserializeObject<NewsResultDTO>(json);
            return dto;
        }
    }
}

