﻿using System;

namespace app22.Core.Services
{
    public static class Service<T>
    {
        private static T _instance;

        public static void Register(T instance)
        {
            if (_instance != null)
                throw new Exception("Сервис такого типа уже существует");
            _instance = instance;
        }

        public static T GetInstance()
        {
            return _instance;
        }

        //replace
    }
}