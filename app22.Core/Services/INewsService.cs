﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using app22.Core.Services.DTOs;

namespace app22.Core.Services
{
    public interface INewsService
    {
        Task<NewsResultDTO> LoadNews(string search, CancellationToken token = default);
    }
}

