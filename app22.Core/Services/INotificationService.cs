﻿using System;
using System.Threading.Tasks;

namespace app22.Core.Services
{
    public interface INotificationService
    {
        public Task ShowError(string message);
    }
}

