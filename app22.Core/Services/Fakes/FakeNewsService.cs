﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using app22.Core.Services.DTOs;

namespace app22.Core.Services.Fakes
{
    public class FakeNewsService : INewsService
    {
        public List<NewsDTO> News;
        public FakeNewsService()
        {
            News = new List<NewsDTO>()
            {
                new NewsDTO
                {
                    Title = "Machenike F117-7 Plus 2022",
                    Description = "Производитель ноутбуков Machenike представил новинку для геймеров",
                    Content = "Производитель ноутбуков Machenike представил новинку для геймеров — модель F117-7 Plus 2022 с дискретной графикой RTX 3060. По случаю распродажи 11.11 приобрести мощную новинку с большим экраном на AliExpress можно по специальной цене."
                },
                new NewsDTO
                {
                    Title = "Учёные выяснили, какое растение стоит первым выращивать на Марсе",
                    Description = "Согласно результатам недавнего исследования микробиологов из Университета штата Айова",
                    Content = "Согласно результатам недавнего исследования микробиологов из Университета штата Айова, сельскохозяйственную деятельность на Красной планете стоило бы начинать вовсе не с картофеля, как это было показано в знаменитом фильме Ридли Скотта «Марсианин»."
                }
            };
        }

        public async Task<NewsResultDTO> LoadNews(string search, CancellationToken token = default)
        {
            await Task.Delay(2000);
            return new NewsResultDTO
            {
                Status = "ok",
                Articles = News,
                TotalResults = 2
            };
        }
    }
}