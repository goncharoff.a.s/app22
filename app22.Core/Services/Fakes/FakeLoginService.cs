﻿using System;
using System.Threading.Tasks;
using app22.Core.Services.DTOs;

namespace app22.Core.Services.Fakes
{
    public class FakeLoginService : ILoginService
    {
        UserDTO _userDTO;
        public FakeLoginService()
        {
            _userDTO = new UserDTO
            {
                Id = 1,
                Login = "123",
                Name = "Ваня"
            };
        }


        async Task<UserDTO> ILoginService.Login(string login, string pass)
        {
            await Task.Delay(2000);

            if (_userDTO.Login == login)
                return _userDTO;

            throw new Exception("Пользователь не найден");
        }
    }
}

