﻿using System;
using System.Threading.Tasks;

namespace app22.Core.Services
{
	public interface ILocalStorage
	{
        Task SaveSearch(string search);
        Task<string> GetSearch();

    }
}

