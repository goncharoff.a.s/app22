﻿using System;
using System.Threading.Tasks;
using app22.Core.Services.DTOs;

namespace app22.Core.Services
{
    public interface ILoginService
    {
        public Task<UserDTO> Login(string login, string pass);
    }
}

